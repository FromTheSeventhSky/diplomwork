package ru.fromtheseventhsky.mydyplom;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ru.fromtheseventhsky.mydyplom.utility.ApiGetDB;


public class LoginActivity extends Activity {

    private Button mSendData;
    private EditText mNameView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private ApiGetDB mNetwork;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(PreferenceManager.getDefaultSharedPreferences(this).getBoolean("logined",false)){
            if(getSharedPreferences("visit_prefs", MODE_PRIVATE).getBoolean("visitStarted",false)) {
                startActivity(new Intent(this, VisitActivity.class));
                finish();
            } else {
                startActivity(new Intent(this, MainActivity.class));
                finish();
            }
        }
        setContentView(R.layout.login_activity);
        mNameView = (EditText) findViewById(R.id.enterName);
        mPasswordView = (EditText) findViewById(R.id.enterPass);
        mSendData = (Button) findViewById(R.id.email_sign_in_button);
        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        mNetwork = new ApiGetDB(this, mNameView, mPasswordView, mProgressView, mLoginFormView);

        mNameView.addTextChangedListener(new TextWatcher(){
            public void afterTextChanged(Editable s) {
                //Toast.makeText(getApplicationContext(), "afterTextChanged", 1).show();
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){

            }
            public void onTextChanged(CharSequence s, int start, int before, int count){
                mNameView.setError(null);
            }
        });
        mPasswordView.addTextChangedListener(new TextWatcher(){
            public void afterTextChanged(Editable s) {
                //Toast.makeText(getApplicationContext(), "afterTextChanged", 1).show();
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){

            }
            public void onTextChanged(CharSequence s, int start, int before, int count){
                mPasswordView.setError(null);
            }
        });



        mSendData.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(!mNetwork.isNetworkAvailable()){
                    Toast.makeText(getApplicationContext(), getResources()
                                    .getText(R.string.warning_network_unavailable),
                            Toast.LENGTH_LONG).show();
                    return;
                }
                boolean error = false;
                if(mNameView.getEditableText().toString().length()==0){
                    error = true;
                    mNameView.setError(getText(R.string.warning_0));
                }
                if(mPasswordView.getEditableText().toString().length()==0){
                    error = true;
                    mPasswordView.setError(getText(R.string.warning_0));
                }
                if(mPasswordView.getEditableText().toString().length()<4&&mPasswordView.getEditableText().toString().length()>0){
                    error = true;
                    mPasswordView.setError(getText(R.string.warning_pass_4));
                }
                if(error == true) return;

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(mPasswordView.getWindowToken(), 0);

                mNetwork.sendRequest.execute();
                mLoginFormView.setVisibility(View.GONE);
                mProgressView.setVisibility(View.VISIBLE);

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
