package ru.fromtheseventhsky.mydyplom.loaders;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.content.AsyncTaskLoader;

import java.util.ArrayList;

import ru.fromtheseventhsky.mydyplom.utility.DataInChains;
import ru.fromtheseventhsky.mydyplom.utility.DataInGoods;

public class LoaderGoods extends AsyncTaskLoader<ArrayList<Cursor>> {

    private SQLiteDatabase myDiplomaDB;
    private String chain_id, category_id;
    private ArrayList<DataInGoods> dataInGoods;
    private DataInGoods dataInGoodsItem;
    private ArrayList<DataInChains> dataInChains;
    private ArrayList<Integer> indexes;
    private ArrayList<Integer> quant;
    private Context context;
    private int loaderId;

    private ArrayList<Cursor>  cursors;

    public LoaderGoods(Context context, SQLiteDatabase myDiplomaDB, String chain_id, String category_id, int loaderId) {
        super(context);
        this.context = context;
        this.myDiplomaDB = myDiplomaDB;

        this.loaderId = loaderId;

        cursors = new ArrayList<Cursor>();

        this.chain_id=chain_id;
        this.category_id=category_id;
    }

    public LoaderGoods(Context context, SQLiteDatabase myDiplomaDB, DataInGoods dataInGoodsItem, int loaderId) {
        super(context);
        this.context = context;
        this.myDiplomaDB = myDiplomaDB;

        this.dataInGoodsItem = dataInGoodsItem;

        this.loaderId = loaderId;
    }

    public LoaderGoods(Context context, SQLiteDatabase myDiplomaDB, ArrayList<Integer> quant,
                       ArrayList<DataInGoods> dataInGoods, ArrayList<Integer> indexes,
                       ArrayList<DataInChains> dataInChains, int loaderId) {
        super(context);
        this.context = context;
        this.myDiplomaDB = myDiplomaDB;
        this.quant = quant;
        this.loaderId = loaderId;

        this.dataInGoods = dataInGoods;
        this.indexes = indexes;
        this.dataInChains = dataInChains;
    }

    public LoaderGoods(Context context, SQLiteDatabase myDiplomaDB, int loaderId) {
        super(context);
        this.context = context;
        this.myDiplomaDB = myDiplomaDB;
        cursors = new ArrayList<Cursor>();
        this.loaderId = loaderId;
    }


    public ArrayList<Cursor> fillDataInGoods() {
        cursors.add(myDiplomaDB.query("goods", null, null, null, null, null, null));
        cursors.add(myDiplomaDB.query("goods2chain", null, "chain_id=? and category_id=?", new String[] {chain_id, category_id}, null, null, null));
        return cursors;
    }
    public ArrayList<Cursor> getListData() {
        ArrayList<Cursor> cursors1 = new ArrayList<Cursor>();

        cursors1.add(myDiplomaDB.query("reports", null, "visit_id=?", new String[] {
                String.valueOf(context.getSharedPreferences("visit_prefs", context.MODE_PRIVATE)
                        .getLong("start", 0))}, null, null, null));
        return cursors1;
    }

    public void uploadStartContent(){
        for (int i = 0; i < dataInGoods.size(); i++) {
            ContentValues cv = new ContentValues();

            //предварительное забитие таблицы РЕПОРТС инфой о стартовой цене и тд
            int index = indexes.indexOf(dataInGoods.get(i).getId());
            cv.put("visit_id", context.getSharedPreferences("visit_prefs", context.MODE_PRIVATE).getLong("start", 0));
            cv.put("goods_id", dataInGoods.get(i).getId());
            cv.put("factory_id", dataInGoods.get(i).getFactory_id());
            cv.put("report_chain_id", dataInChains.get(index).getChain_id());
            cv.put("report_category_id", dataInChains.get(index).getCategory_id());
            cv.put("basic_count", quant.get(i));
            cv.put("basic_price", dataInGoods.get(i).getPrice());
            cv.put("count", 0);
            cv.put("real_price", 0);
            myDiplomaDB.insert("reports", null, cv);
        }

    }

    public int getDataInGoodsCount(DataInGoods item){
        if(item.isOnStock() == false)
            return 0;
        else{
            if(item.isOnShelf() == false)
                return 1;
            else
                return 2;
        }

    }

    public void uploadFinishContent(){
        ContentValues cv = new ContentValues();
        cv.put("count", getDataInGoodsCount(dataInGoodsItem));
        cv.put("real_price", dataInGoodsItem.getPrice());
        myDiplomaDB.update("reports", cv, "id = ?", new String[]{""+dataInGoodsItem.getIdRow()});

    }


    @Override
    public ArrayList<Cursor> loadInBackground() {
        switch(loaderId){
            case 0:
                return fillDataInGoods();
            case 1:
                uploadStartContent();
                return null;
            case 2:
                uploadFinishContent();
                return null;
            case 3:

                return getListData();
            default:
                return null;
        }

    }

}
