package ru.fromtheseventhsky.mydyplom.loaders;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.content.AsyncTaskLoader;

import java.util.ArrayList;
import java.util.Calendar;

public class LoaderLocalDatabase extends AsyncTaskLoader<Cursor> {

    private SQLiteDatabase uploadDB;
    private int id;
    private ContentValues cv;
    private ArrayList<Integer> integers;
    private int row_id_in_visits;
    private int factory_id;

    public LoaderLocalDatabase(Context context, SQLiteDatabase uploadDB, ContentValues cv, int id) {
        super(context);
        row_id_in_visits = context.getSharedPreferences("visit_prefs", context.MODE_PRIVATE).getInt("visit_before_upload",1);
        this.uploadDB = uploadDB;
        this.cv = cv;
        this.id = id;
    }
    public LoaderLocalDatabase(Context context, SQLiteDatabase uploadDB, ArrayList<Integer> integers, int id) {
        super(context);
        row_id_in_visits = context.getSharedPreferences("visit_prefs", context.MODE_PRIVATE).getInt("visit_before_upload",1);
        factory_id = context.getSharedPreferences("visit_prefs", context.MODE_PRIVATE).getInt("factory_id",1);
        this.uploadDB = uploadDB;
        this.integers = integers;
        this.id = id;
    }

    public LoaderLocalDatabase(Context context, SQLiteDatabase uploadDB, int id) {
        super(context);
        row_id_in_visits = context.getSharedPreferences("visit_prefs", context.MODE_PRIVATE).getInt("tp_id",1);
        factory_id = context.getSharedPreferences("visit_prefs", context.MODE_PRIVATE).getInt("factory_id",1);
        this.uploadDB = uploadDB;
        this.id = id;
    }

    private void uploadStart(){
        uploadDB.insert("visits", null, cv);
    }

    private void uploadVisits(){
        uploadDB.update("visits", cv, "id = ?", new String[]{String.valueOf(row_id_in_visits)});
    }
    private void uploadReportsStatus (){
        ContentValues cv = new ContentValues();
        int count, i, j;
        boolean haveBreach = false;
        count = integers.size();
        for(i = 1, j = 0; j < count; i++, j++) {
            cv.clear();
            if(integers.get(j)==0){
                haveBreach = true;
            }
            cv.put("status", integers.get(j));
            uploadDB.update("reports", cv, "id = ?", new String[]{String.valueOf(i)});
        }

        cv.clear();
        j = 0;
        for(i = 0; i< count; i++){
            if(integers.get(i)==0){
                j++;
            }
        }
        i = j*100/count;

        if(haveBreach){
            cv.put("status", 0);
        } else {
            cv.put("status", 1);
        }

        cv.put("done_percent", i);
        cv.put("factory_id", factory_id);

        uploadDB.update("visits", cv, "id = ?", new String[]{String.valueOf(row_id_in_visits)});

    }

    private void uploadVisitTime(){
        ContentValues cv = new ContentValues();
        cv.put("lastvisit", Calendar.getInstance().getTimeInMillis()/1000);

        uploadDB.update("tp", cv, "id = ?", new String[]{String.valueOf(row_id_in_visits)});
    }


    private Cursor getData(){
        return uploadDB.query("reports", null, null, null, null, null, null);
    }

    @Override
    public Cursor loadInBackground() {

        switch(id){
            case 0:
                uploadStart();
                return null;
            case 1:
                uploadVisits();
                return null;
            case 2:
                return getData();
            case 3:
                uploadReportsStatus();
                return null;
            case 5:
                uploadVisitTime();
                return null;
        }
        return null;
    }
}
