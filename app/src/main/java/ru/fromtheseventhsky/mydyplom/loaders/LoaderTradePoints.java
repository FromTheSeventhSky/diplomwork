package ru.fromtheseventhsky.mydyplom.loaders;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.content.AsyncTaskLoader;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

import ru.fromtheseventhsky.mydyplom.utility.DataInTP;

public class LoaderTradePoints extends AsyncTaskLoader<ArrayList<DataInTP>> {
    private Cursor c;
    private SQLiteDatabase myDiplomaDB;

    public LoaderTradePoints(Context context, SQLiteDatabase myDiplomaDB) {
        super(context);
        this.myDiplomaDB = myDiplomaDB;

    }

    public ArrayList<DataInTP> fillArrayListTP() {


        c = myDiplomaDB.query("tp", null, null, null, null, null, null);
        c.moveToFirst();
        long period, lastvisit, curTime = Calendar.getInstance().getTimeInMillis();
        ArrayList<DataInTP> dataInTP = new ArrayList<DataInTP>();

        if (c.getCount() != 0)
            do {
                dataInTP.add(new DataInTP(
                        c.getInt(c.getColumnIndex("id")),
                        c.getInt(c.getColumnIndex("merch_id")),
                        c.getInt(c.getColumnIndex("chain_id")),
                        c.getInt(c.getColumnIndex("category_id")),
                        (period = c.getInt(c.getColumnIndex("period"))),
                        (lastvisit = c.getInt(c.getColumnIndex("lastvisit"))),
                        c.getString(c.getColumnIndex("name")),
                        c.getString(c.getColumnIndex("address")),
                        c.getString(c.getColumnIndex("chain_name")),
                        c.getString(c.getColumnIndex("category_name")),
                        c.getFloat(c.getColumnIndex("lon")),
                        c.getFloat(c.getColumnIndex("lat")),
                        (curTime > (lastvisit + period * (long) 86400) * (long) 1000) ? 0 : 1

                ));

            } while (c.moveToNext());
        c.close();
        Collections.sort(dataInTP, new Comparator<DataInTP>() {
            @Override
            public int compare(DataInTP obj1, DataInTP ob2) {

                if (obj1.lastvisit > ob2.lastvisit) {//первая дата более свежая, вторая давнишняя
                    if (obj1.hasVisited == ob2.hasVisited)
                        return -1;//обе либо посещены либо не посещены, первое выше второго
                    if (obj1.hasVisited < ob2.hasVisited)
                        return -1;//первая не посещена вторая посещена, первая выше
                    if (obj1.hasVisited > ob2.hasVisited)
                        return 1;//первая посещена, вторая нет, вторая выше
                }
                if (obj1.lastvisit < ob2.lastvisit) {//вторая дата более свежая, первая давнишняя
                    if (obj1.hasVisited == ob2.hasVisited)
                        return 1;
                    if (obj1.hasVisited < ob2.hasVisited)
                        return -1;
                    if (obj1.hasVisited > ob2.hasVisited)
                        return 1;
                }
                if (obj1.lastvisit == ob2.lastvisit) {//вторая дата более свежая, первая давнишняя
                    if (obj1.hasVisited == ob2.hasVisited)
                        return 0;
                    if (obj1.hasVisited < ob2.hasVisited)
                        return -1;
                    if (obj1.hasVisited > ob2.hasVisited)
                        return 1;
                }
                return 0;
            }
        });
        return dataInTP;
    }


    @Override
    public ArrayList<DataInTP> loadInBackground() {
        return fillArrayListTP();
    }

}
