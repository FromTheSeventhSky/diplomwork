package ru.fromtheseventhsky.mydyplom;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import ru.fromtheseventhsky.mydyplom.fragments.CameraFragment;
import ru.fromtheseventhsky.mydyplom.fragments.VisitFragment;
import ru.fromtheseventhsky.mydyplom.fragments.WriteCommentFragment;
import ru.fromtheseventhsky.mydyplom.loaders.LoaderLocalDatabase;
import ru.fromtheseventhsky.mydyplom.utility.DataInGoods;
import ru.fromtheseventhsky.mydyplom.utility.MyDatabase;
import ru.fromtheseventhsky.mydyplom.utility.utils;


public class VisitActivity extends ActionBarActivity implements CameraFragment.PhotoReturnListener,
        VisitFragment.VisitInterface, LoaderManager.LoaderCallbacks<Cursor>{


    private boolean havePhoto;
    private ContentValues cv;
    private CameraFragment cf;
    private VisitFragment vf;
    private WriteCommentFragment wf;

    private MyDatabase myDatabase;
    private SQLiteDatabase uploadDB;

    LoaderManager.LoaderCallbacks<Cursor> callback;

    private boolean cfStartEnabled;
    private boolean cfFinishEnabled;
    private long back_pressed;


    private int chain_id;
    private int category_id;

    ArrayList<Integer> goodStatus;
    ArrayList<Bitmap> startPhoto;
    ArrayList<Bitmap> finishPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.visit_activity);
        havePhoto = getIntent().getBooleanExtra("HAVE_PHOTO", true);
        getSupportActionBar().setTitle(R.string.title_visit);

        Bundle extras = getIntent().getExtras();
        cv = new ContentValues();
        myDatabase = new MyDatabase(this, "db.sqlite", null, 1);

        uploadDB = myDatabase.getWritableDatabase();

        callback = this;
        utils.hideGPSNotifications();
        utils.openGPSConnection(this);

        float lat;
        float lon;
        long start;
        if (extras != null) {
            int tp_id = extras.getInt("tp_id");
            chain_id = extras.getInt("chain_id");
            category_id = extras.getInt("category_id");
            start = extras.getLong("start");
            lon = extras.getFloat("lon_start");
            lat = extras.getFloat("lat_start");


            SharedPreferences shp = getSharedPreferences("visit_prefs", MODE_PRIVATE);
            SharedPreferences.Editor editShp = shp.edit();
            cv.clear();

            cv.put("tp_id", tp_id);
            cv.put("lon_start", lon);
            cv.put("lat_start", lat);
            cv.put("start", start);
            cv.put("month_year" ,new SimpleDateFormat("MMM-yy", Locale.getDefault())
                    .format(new Date(Calendar.getInstance().getTimeInMillis())));

            getSupportLoaderManager().initLoader(0, null, this).forceLoad();

            editShp.putBoolean("visitStarted",true);
            editShp.putInt("tp_id", tp_id);
            editShp.putInt("chain_id", chain_id);
            editShp.putInt("category_id", category_id);
            editShp.putLong("start" , start);
            editShp.commit();
        } else {
            SharedPreferences shp = getSharedPreferences("visit_prefs", MODE_PRIVATE);
            chain_id = shp.getInt("chain_id", 0);
            category_id = shp.getInt("category_id", 0);
        }
        startPhoto = null;
        finishPhoto = null;
    }

    void finishVisit(){
        SharedPreferences shp = getSharedPreferences("visit_prefs", MODE_PRIVATE);
        SharedPreferences.Editor editShp = shp.edit();
        editShp.putBoolean("visitStarted", false);
        editShp.putBoolean("photo_start_completed",false);
        editShp.putBoolean("goods_check_completed",false);
        editShp.putBoolean("photo_finish_completed",false);
        editShp.putBoolean("comment_completed",false);
        editShp.putBoolean("done_percent_continue",false);
        editShp.putBoolean("base_goods_uploaded", false);

        editShp.putInt("visit_before_upload", shp.getInt("visit_before_upload",1)+1);

        editShp.putInt("chain_id", 0);
        editShp.putInt("category_id", 0);
        editShp.apply();
    }

    @Override
    protected void onResume() {
        super.onResume();

        utils.addGPSUpdates();

        if(cf == null)
            cf = CameraFragment.newInstance();
        if(vf == null)
            vf = VisitFragment.newInstance(chain_id, category_id, uploadDB);
        if(wf == null)
            wf = WriteCommentFragment.newInstance();

        FragmentManager fragmentManager = getSupportFragmentManager();

        //ЕСЛИ ФОТОГРАФИИ НАЧАЛА ПОСЕЩЕНИЯ НЕ СДЕЛАНЫ, ТО ОТКРЫТЬ КАМЕРУ И СДЕЛАТЬ ИХ
        if(!getSharedPreferences("visit_prefs", MODE_PRIVATE).getBoolean("photo_start_completed",false)) {
            cfStartEnabled = true;
            getSupportActionBar().hide();
            fragmentManager.beginTransaction()
                    .replace(R.id.vFrameForFragment, cf)
                    .commit();
            //ИНАЧЕ, ЕСЛИ СДЕЛАНЫ, ТО ОТКРЫТЬ ФРАГМЕНТ С ЗАПИСЯМИ
        } else {
            if(!getSharedPreferences("visit_prefs", MODE_PRIVATE).getBoolean("goods_check_completed",false)) {
                fragmentManager.beginTransaction()
                        .replace(R.id.vFrameForFragment, vf)
                        .commit();
                //НО ЕСЛИ И ФОТО ГОТОВЫ, И ВЫКЛАДКА ПРОВЕРЕНА, ТО ОТКРЫТЬ КАМЕРУ ДЛЯ ФИНАЛЬНОЙ СЪЕМКИ
            } else {
                if(!getSharedPreferences("visit_prefs", MODE_PRIVATE).getBoolean("photo_finish_completed",false)) {
                    cfFinishEnabled = true;
                    getSupportActionBar().hide();
                    fragmentManager.beginTransaction()
                            .replace(R.id.vFrameForFragment, cf)
                            .commit();
                } else {
                    if (!getSharedPreferences("visit_prefs", MODE_PRIVATE).getBoolean("comment_completed", false)) {
                        if(!getSharedPreferences("visit_prefs", MODE_PRIVATE)
                                .getBoolean("done_percent_continue",false)){
                            getSupportLoaderManager()
                                    .initLoader(2, null, callback).forceLoad();
                        }
                        fragmentManager.beginTransaction()
                                .replace(R.id.vFrameForFragment, wf)
                                .commit();
                    }
                }
            }
        }

    }

    @Override
    public void onBackPressed() {
        if (cfStartEnabled) {
            cf.closePreview();
            return;
        }

        if (back_pressed + 2000 > System.currentTimeMillis()) {
            super.onBackPressed();
        } else {
            Toast.makeText(getBaseContext(), "Нажмите еще раз для выхода", Toast.LENGTH_SHORT).show();
        }
        back_pressed = System.currentTimeMillis();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_visit, menu);
        menu.findItem(R.id.action_refresh).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_refresh) {
            if (!getSharedPreferences("visit_prefs", MODE_PRIVATE).getBoolean("goods_check_completed", false)) {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.visit_popup_title)
                        .setPositiveButton(R.string.yes,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        cfFinishEnabled = true;
                                        SharedPreferences shp = getSharedPreferences("visit_prefs", MODE_PRIVATE);
                                        SharedPreferences.Editor editShp = shp.edit();
                                        editShp.putBoolean("goods_check_completed", true);
                                        editShp.commit();

                                        if(!shp.getBoolean("done_percent_continue",false)){
                                            getSupportLoaderManager()
                                                    .initLoader(2, null, callback).forceLoad();
                                        }

                                        getSupportActionBar().hide();
                                        getSupportFragmentManager().beginTransaction()
                                                .replace(R.id.vFrameForFragment, cf)
                                                .commit();
                                    }
                                })
                        .setNegativeButton(R.string.no,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                }).show();
                return true;
            } else if (!getSharedPreferences("visit_prefs", MODE_PRIVATE).getBoolean("comment_completed", false)) {

                new AlertDialog.Builder(this)
                        .setTitle(R.string.visit_popup_comment_title)
                        .setPositiveButton(R.string.yes,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        cv.clear();
                                        cv.put("end", Calendar.getInstance().getTimeInMillis()/1000);
                                        cv.put("lon_end", 1.9);
                                        cv.put("lat_end", 1.9);
                                        cv.put("comment", wf.getCommentText());

                                        getSupportLoaderManager()
                                                .initLoader(1, null, callback).forceLoad();

                                        getSupportLoaderManager()
                                                .initLoader(5, null, callback).forceLoad();

                                        finishVisit();

                                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                        finish();

                                    }
                                })
                        .setNegativeButton(R.string.no,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                }).show();

                /*if (utils.checkGPSEnabled()) {
                    if (utils.checkLocationAvailable()) {
                        new AlertDialog.Builder(this)
                                .setTitle(R.string.visit_popup_comment_title)
                                .setPositiveButton(R.string.yes,
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                ContentValues cv = new ContentValues();
                                                cv.put("end", Calendar.getInstance().getTimeInMillis());
                                                cv.put("lon_end", utils.getLongitude());
                                                cv.put("lat_end", utils.getLatitude());
                                                cv.put("comment", wf.getCommentText());
                                                finishVisit();
                                                getSupportLoaderManager()
                                                        .initLoader(1, null, callback).forceLoad();

                                            }
                                        })
                                .setNegativeButton(R.string.no,
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        }).show();

                    } else {
                        Toast.makeText(this, R.string.location_wait_loc, Toast.LENGTH_LONG).show();
                    }
                } else {
                    new AlertDialog.Builder(this)
                            .setTitle(R.string.location_enable)
                            .setPositiveButton(R.string.yes,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            startActivity(new Intent(
                                                    android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                        }
                                    })
                            .setNegativeButton(R.string.no,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    }).show();
                }*/
                return true;
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void PhotoReturn(ArrayList<Bitmap> al) {
        getSupportActionBar().show();

        if(cfStartEnabled)
            if(startPhoto == null) {
                startPhoto = al;

                cfStartEnabled = false;

                cv.clear();

                for(int i = 0; i < 5; i++) {
                    Bitmap tmp = startPhoto.get(i);





                    byte[] bytes;
                    if(tmp!=null) {


                        ByteArrayOutputStream os = new ByteArrayOutputStream();
                        tmp.compress(Bitmap.CompressFormat.JPEG, 50, os);
                        bytes = os.toByteArray();
                        //ByteBuffer byteBuffer = ByteBuffer.allocate(tmp.getByteCount());

                        //tmp.copyPixelsToBuffer(byteBuffer);
                        //bytes = byteBuffer.array();

                        try {
                            os.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        bytes = null;
                    }
                    switch (i){
                        case 0:
                            cv.put("photo_start_1", bytes);
                            break;
                        case 1:
                            cv.put("photo_start_2", bytes);
                            break;
                        case 2:
                            cv.put("photo_start_3", bytes);
                            break;
                        case 3:
                            cv.put("photo_start_4", bytes);
                            break;
                        case 4:
                            cv.put("photo_start_5", bytes);
                            break;
                    }
                }
                al.clear();
                getSupportLoaderManager().initLoader(1, null, this).forceLoad();

                SharedPreferences shp = getSharedPreferences("visit_prefs", MODE_PRIVATE);
                SharedPreferences.Editor editShp = shp.edit();
                editShp.putBoolean("photo_start_completed",true);
                editShp.commit();

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.vFrameForFragment, vf)
                        .commit();
            }
        if(cfFinishEnabled)
            if(finishPhoto == null) {
                finishPhoto = al;
                cfFinishEnabled = false;

                cv.clear();

                for(int i = 0; i < 5; i++) {

                    Bitmap tmp = finishPhoto.get(i);


                    byte[] bytes;
                    if(tmp!=null) {
                        //ByteBuffer byteBuffer = ByteBuffer.allocate(tmp.getByteCount());
                        //tmp.copyPixelsToBuffer(byteBuffer);
                        //bytes = byteBuffer.array();
                        ByteArrayOutputStream os = new ByteArrayOutputStream();
                        tmp.compress(Bitmap.CompressFormat.JPEG, 50, os);
                        bytes = os.toByteArray();

                    } else {
                        bytes = null;
                    }
                    switch (i){
                        case 0:
                            cv.put("photo_end_1", bytes);
                            break;
                        case 1:
                            cv.put("photo_end_2", bytes);
                            break;
                        case 2:
                            cv.put("photo_end_3", bytes);
                            break;
                        case 3:
                            cv.put("photo_end_4", bytes);
                            break;
                        case 4:
                            cv.put("photo_end_5", bytes);
                            break;
                    }
                }
                getSupportLoaderManager().initLoader(1, null, this).forceLoad();

                al.clear();
                SharedPreferences shp = getSharedPreferences("visit_prefs", MODE_PRIVATE);
                SharedPreferences.Editor editShp = shp.edit();
                editShp.putBoolean("photo_finish_completed",true);
                editShp.commit();

                if (!getSharedPreferences("visit_prefs", MODE_PRIVATE)
                        .getBoolean("done_percent_continue", false)) {
                    getSupportLoaderManager()
                            .initLoader(2, null, callback).forceLoad();
                }
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.vFrameForFragment, wf)
                        .commit();
            }
    }

    @Override
    public void getList(ArrayList<DataInGoods> data) {

    }


    @Override
    protected void onPause() {
        super.onPause();
        utils.removeGPSUpdates();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //if(uploadDB != null && uploadDB.isOpen())
        //    uploadDB.close();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch(id){
            case 0:
                return new LoaderLocalDatabase(this, uploadDB, cv, 0);
            case 1:
                return new LoaderLocalDatabase(this, uploadDB, cv, 1);
            case 2:
                return new LoaderLocalDatabase(this, uploadDB, cv, 2);
            case 3:
                return new LoaderLocalDatabase(this, uploadDB, goodStatus, 3);
            case 5:
                return new LoaderLocalDatabase(this, utils.getDB(
                        new File(this.getExternalCacheDir().getAbsolutePath(), "sync.sqlite")), 5);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if(loader.getId()==2) {
            int basic_count;
            int count;
            float basic_price;
            float real_price ;



            goodStatus = new ArrayList<Integer>();

            data.moveToFirst();

            do {
                basic_count = data.getInt(data.getColumnIndex("basic_count"));
                count = data.getInt(data.getColumnIndex("count"));
                basic_price = data.getFloat(data.getColumnIndex("basic_price"));
                real_price = data.getFloat(data.getColumnIndex("real_price"));

                if(count < basic_count || basic_price < real_price){
                    goodStatus.add(0);
                } else {
                    goodStatus.add(1);
                }

            } while(data.moveToNext());

            getSupportLoaderManager().initLoader(3, null, this).forceLoad();

            SharedPreferences shp = getSharedPreferences("visit_prefs", MODE_PRIVATE);
            SharedPreferences.Editor editShp = shp.edit();
            editShp.putBoolean("done_percent_continue",true);
            editShp.commit();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
