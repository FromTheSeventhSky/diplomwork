package ru.fromtheseventhsky.mydyplom.utility;


import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.apache.http.NameValuePair;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.List;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import ru.fromtheseventhsky.mydyplom.R;

public class ApiUploadDB {

    private Activity activity;
    private Button sync;
    private ProgressBar syncDataProgress;
    private BeginTransaction beginTransaction;
    private ArchiveDB archiveDB;
    private SendFile sendFile;
    private CheckTransaction checkTransaction;



    public void sendFile(){
        beginTransaction = new BeginTransaction();
        archiveDB = new ArchiveDB();
        sendFile = new SendFile();
        beginTransaction.execute();
    }

    private class ArchiveDB extends AsyncTask<Void, Void, Integer> {

        @Override
        protected Integer doInBackground(Void... params) {
            return archiveDB();
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(Integer result) {
            if(result != 1){
                syncDataProgress.setVisibility(View.GONE);
                sync.setEnabled(true);
                Toast.makeText(activity, activity.getText(R.string.warning_archive), Toast.LENGTH_LONG).show();
                return;
            }
            syncDataProgress.setProgress(2);
            sendFile.execute();
        }
    }

    private class BeginTransaction extends AsyncTask<Void, Void, Integer> {

        @Override
        protected Integer doInBackground(Void... params) {
            return beginTransaction("http://forafarm.dvaoblaka.ru/fora/startTransaction/");
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(Integer result) {
            if(result != 200){
                sync.setEnabled(true);
                syncDataProgress.setVisibility(View.GONE);
                Toast.makeText(activity, activity.getText(R.string.warning_code_not_200), Toast.LENGTH_LONG).show();
                return;
            }
            syncDataProgress.setProgress(1);
            archiveDB.execute();
        }
    }

    private class SendFile extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            sendData("http://forafarm.dvaoblaka.ru/fora/setData");
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            syncDataProgress.setProgress(3);
            checkTransaction.execute();
        }
    }

    private class CheckTransaction extends AsyncTask<Void, Void, Integer> {
        String uuid = PreferenceManager.getDefaultSharedPreferences(activity).getString("UUID", "");

        File pathToDB = new File(activity.getExternalCacheDir().getAbsolutePath()
                + File.separator + "/upload/" + File.separator
                +  "db.sqlite");

        File pathToZipFile = new File(activity.getExternalCacheDir().getAbsolutePath()
                + File.separator + "/upload/" + File.separator
                +  uuid + ".zip");

        @Override
        protected Integer doInBackground(Void... params) {
            return checkTransaction("http://forafarm.dvaoblaka.ru/fora/checkTransaction/");
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(Integer result) {
            if(result == 200){
                syncDataProgress.setVisibility(View.GONE);
                pathToDB.delete();
                pathToZipFile.delete();
                sync.setEnabled(true);
                Toast.makeText(activity, activity.getText(R.string.warning_code_200), Toast.LENGTH_LONG).show();
                return;
            }
            if(result == 400){
                CountDownTimer timer = new CountDownTimer(2000, 2000) {
                    public void onFinish() {

                    }

                    public void onTick(long millisUntilFinished) {
                        syncDataProgress.setProgress(4);
                        checkTransaction.execute();
                    }
                }.start();
                return;
            }
            Toast.makeText(activity, activity.getText(R.string.warning_code_404), Toast.LENGTH_LONG).show();
            syncDataProgress.setProgress(0);
            pathToZipFile.delete();

            sync.setEnabled(true);
            //beginTransaction.execute();


        }
    }

    public boolean isNetworkAvailable(){
        ConnectivityManager connMgr = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    private Integer beginTransaction(String url_){

        UUID uuid = UUID.randomUUID();
        String str = uuid.toString();
        str = str.replaceAll("[-]*", "");
        url_ += str;

        PreferenceManager.getDefaultSharedPreferences(activity).edit().putString("UUID", str).commit();


        int responseCode=0;
        try {
            URL url = new URL(url_);

            HttpURLConnection httpConnection = (HttpURLConnection) url
                    .openConnection();

            httpConnection.setRequestMethod("GET");
            httpConnection.connect();

            responseCode = httpConnection.getResponseCode();


            if(responseCode==HttpURLConnection.HTTP_FORBIDDEN){
                return responseCode;
            }
        } catch (Exception e) {
            Log.d("Exception", e.getLocalizedMessage());
        }

        return responseCode;
    }

    private void sendData(String url_){
        OutputStream outputStream = null;
        File pathToZip;
        String uuid = PreferenceManager.getDefaultSharedPreferences(activity).getString("UUID", "");
        String md5 = PreferenceManager.getDefaultSharedPreferences(activity).getString("md5","");

        try {

            URL url = new URL(url_);


            HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
            String boundary = "===" + System.currentTimeMillis() + "===";
            String lineEnd = "\r\n";
            String twoHyphens = "--";


            httpConnection.setRequestMethod("POST");
            httpConnection.setDoInput(true);
            httpConnection.setDoOutput(true);
            httpConnection.setRequestProperty("Connection", "Keep-Alive");
            httpConnection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

            outputStream = httpConnection.getOutputStream();

            String zipName = new StringBuilder().append(uuid).toString();
            pathToZip = new File(activity.getExternalCacheDir().getAbsolutePath()
                    + File.separator + "/upload/" + File.separator
                    + uuid + ".zip");


            PrintWriter writer = new PrintWriter(new OutputStreamWriter(outputStream, "UTF-8"), true);

            String fileName = pathToZip.getName();


            writer.append("--" + boundary).append(lineEnd);
            writer.append("Content-Disposition: form-data; name=\"md5\"").append(lineEnd);
            writer.append("Content-Type: text/plain; charset=UTF-8").append(lineEnd);
            writer.append(lineEnd).append(md5).append(lineEnd).flush();

            writer.append("--" + boundary).append(lineEnd);
            writer.append(
                    "Content-Disposition: form-data; name=\"sync\"; filename=\"" + fileName + "\"")
                    .append(lineEnd);
            writer.append(
                    "Content-Type: "
                            + URLConnection.guessContentTypeFromName(fileName))
                    .append(lineEnd);
            writer.append("Content-Transfer-Encoding: binary").append(lineEnd);
            writer.append(lineEnd);
            writer.flush();

            FileInputStream fileInputStream = new FileInputStream(pathToZip);

            byte[] buffer = new byte[1024];

            int length;

            while ((length = fileInputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, length);
            }
            outputStream.flush();
            fileInputStream.close();

            writer.append(lineEnd);
            writer.flush();
            writer.append("--" + boundary + "--").append(lineEnd).flush();

            httpConnection.connect();

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
    }

    private Integer archiveDB(){
        String uuid = PreferenceManager.getDefaultSharedPreferences(activity).getString("UUID", "");
        File pathToDB = new File(activity.getExternalCacheDir().getAbsolutePath()
                + File.separator + "/upload/" + File.separator
                +  "db.sqlite");
        String pathToZipFile = activity.getExternalCacheDir().getAbsolutePath()
                + File.separator + "/upload/" + File.separator
                +  uuid + ".zip";

        try {
            byte[] buffer = new byte[1024];

            FileOutputStream fileOutputStream = new FileOutputStream(pathToZipFile);
            ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream);
            FileInputStream fileInputStream = new FileInputStream(pathToDB);

            zipOutputStream.putNextEntry(new ZipEntry(pathToDB.getName()));

            int length;

            while ((length = fileInputStream.read(buffer)) > 0) {
                zipOutputStream.write(buffer, 0, length);
            }

            zipOutputStream.closeEntry();

            fileInputStream.close();
            zipOutputStream.close();

        } catch (IOException ioe) {
            System.out.println("Error creating zip file" + ioe);
        }




        return 1;
    }


    private Integer checkTransaction(String url_){

        url_ += PreferenceManager.getDefaultSharedPreferences(activity).getString("UUID", "");


        int responseCode=0;
        try {
            URL url = new URL(url_);

            HttpURLConnection httpConnection = (HttpURLConnection) url
                    .openConnection();

            httpConnection.setRequestMethod("GET");
            httpConnection.connect();

            responseCode = httpConnection.getResponseCode();


            if(responseCode==HttpURLConnection.HTTP_FORBIDDEN){
                return responseCode;
            }
        } catch (Exception e) {
            Log.d("Exception", e.getLocalizedMessage());
        }

        return responseCode;
    }

    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    public ApiUploadDB(Activity activity, Button sync, ProgressBar syncDataProgress){
        this.syncDataProgress = syncDataProgress;
        this.sync = sync;
        this.activity = activity;

        checkTransaction = new CheckTransaction();
        syncDataProgress.setVisibility(View.VISIBLE);
        syncDataProgress.setMax(5);
        syncDataProgress.setProgress(0);
    }

}
