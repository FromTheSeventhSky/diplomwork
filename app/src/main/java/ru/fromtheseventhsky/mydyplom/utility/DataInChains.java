package ru.fromtheseventhsky.mydyplom.utility;

/**
 * Created by 123 on 17.04.2015.
 */
public class DataInChains {

    private int goods_id;		//идентификатор товара
    private int chain_id;		//идентификатор сети
    private int category_id;	//идентификатор категории
    private int quant;	    	//количество, но в данном случае это статус: 0 - нет ни в налчии, ни на полке, 1 - есть в наличии, нет на полке, 2 - есть в наличии и на полке

    public DataInChains(int goods_id, int chain_id, int category_id, int quant){
        this.goods_id = goods_id;
        this.chain_id = chain_id;
        this.category_id = category_id;
        this.quant = quant;
    }


    public int getCategory_id() {
        return category_id;
    }

    public int getGoods_id() {
        return goods_id;
    }

    public int getChain_id() {
        return chain_id;
    }

    public int getQuant() {
        return quant;
    }
}
