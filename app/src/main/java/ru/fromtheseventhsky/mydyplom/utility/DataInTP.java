package ru.fromtheseventhsky.mydyplom.utility;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * tp - торговые точки
 */

/*
id	integer primary key autoincrement	идентификатор торговой точки
name	text	название торговой точки
address	text	адрес
merch_id	integer	идентификатор мерча
chain_id	integer	идентификатор сети
chain_name	text	имя сети
category_id	integer	категория в сети
category_name	text	название категории в сети
period	integer	период через который нужно повторить посещение точки
lon	float	долгота
lat	float	широта
lastvisit	integer	дата/время последнего визита


 */
public class DataInTP implements Parcelable {
    public int id;
    public int merch_id;
    public int chain_id;
    public int category_id;
    public long period;
    public long lastvisit;
    public String name;
    public String address;
    public String chain_name;
    public String category_name;
    public double lon;
    public double lat;
    public int hasVisited;
    public DataInTP(
            int _id,
            int _merch_id,
            int _chain_id,
            int _category_id,
            long _period,
            long _lastvisit,
            String _name,
            String _address,
            String _chain_name,
            String _category_name,
            double _lon,
            double _lat,
            int _hasVisited
    ) {
        id = _id;
        merch_id = _merch_id;
        chain_id = _chain_id;
        category_id = _category_id;
        period = _period;
        lastvisit = _lastvisit;
        name = _name;
        address = _address;
        chain_name = _chain_name;
        category_name = _category_name;
        lon = _lon;
        lat = _lat;
        hasVisited = _hasVisited;
    }

    public DataInTP(Parcel in) {
        id = in.readInt();
        merch_id = in.readInt();
        chain_id = in.readInt();
        category_id = in.readInt();
        period = in.readLong();
        lastvisit = in.readLong();
        name = in.readString();
        address = in.readString();
        chain_name = in.readString();
        category_name = in.readString();
        lon = in.readDouble();
        lat = in.readDouble();
        hasVisited = in.readInt();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(merch_id);
        dest.writeInt(chain_id);
        dest.writeInt(category_id);
        dest.writeLong(period);
        dest.writeLong(lastvisit);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(chain_name);
        dest.writeString(category_name);
        dest.writeDouble(lon);
        dest.writeDouble(lat);
        dest.writeInt(hasVisited);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<DataInTP> CREATOR = new Parcelable.Creator<DataInTP>() {
        @Override
        public DataInTP createFromParcel(Parcel in) {
            return new DataInTP(in);
        }

        @Override
        public DataInTP[] newArray(int size) {
            return new DataInTP[size];
        }
    };
}
