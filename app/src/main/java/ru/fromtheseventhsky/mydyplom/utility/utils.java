package ru.fromtheseventhsky.mydyplom.utility;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.Toast;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import ru.fromtheseventhsky.mydyplom.R;

/**
 * Various classes for comfortable work
 */
public class utils {

    private static SQLiteDatabase myDiplomaDB;

    private static LocationListener locationListener = null;
    private static LocationManager locationManager = null;
    private static Location location = null;
    private static boolean showNotify;

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }



    public static double getRadius(double a, double b){
        return Math.sqrt(a*a+b*b);
    }

    public static SQLiteDatabase getDB(File dbFile) {
        if (myDiplomaDB == null || !myDiplomaDB.isOpen())
            myDiplomaDB = SQLiteDatabase.openDatabase(dbFile.getAbsolutePath(), null, SQLiteDatabase.OPEN_READWRITE);
        return myDiplomaDB;
    }
    public static void closeDB(){
        if (myDiplomaDB != null && myDiplomaDB.isOpen())
            myDiplomaDB.close();
    }

    public static void openGPSConnection(final Context ctx){
        if(locationManager==null) {
            locationManager = (LocationManager) ctx.getSystemService(ctx.LOCATION_SERVICE);
        }
        if(locationListener==null){
            locationListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location l) {
                    if(showNotify)
                        Toast.makeText(ctx, R.string.location_loc, Toast.LENGTH_LONG).show();
                    location = l;
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {
                    if(showNotify)
                        Toast.makeText(ctx, R.string.location_service, Toast.LENGTH_LONG).show();
                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            };
        }
    }

    public static double getLatitude(){
        if(location != null)
            return location.getLatitude();
        return 0;
    }
    public static double getLongitude(){
        if(location != null)
            return location.getLongitude();
        return 0;
    }

    public static void showGPSNotifications(){
        showNotify = true;
    }
    public static void hideGPSNotifications(){
        showNotify = false;
    }

    public static void addGPSUpdates(){
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 100000, 10, locationListener);
    }

    public static void removeGPSUpdates(){
        locationManager.removeUpdates(locationListener);
    }

    public static boolean checkGPSEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static boolean checkLocationAvailable() {
        return location==null?false:true;
    }

}
