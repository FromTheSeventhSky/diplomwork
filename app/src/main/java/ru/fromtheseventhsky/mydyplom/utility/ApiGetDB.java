package ru.fromtheseventhsky.mydyplom.utility;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import ru.fromtheseventhsky.mydyplom.MainActivity;
import ru.fromtheseventhsky.mydyplom.R;

/**
 * Взаимодействие с апи
 */
public class ApiGetDB {
    private Activity activity;
    private EditText name;
    private EditText pass;
    private View loadLay;
    private View inputLay;
    public HttpAsyncTask sendRequest;

    public boolean isNetworkAvailable(){
        ConnectivityManager connMgr = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }


    public class HttpAsyncTask extends AsyncTask<Void, Void, String> {


        @Override
        protected String doInBackground(Void... params) {

            return POST("http://forafarm.dvaoblaka.ru/fora/getData");
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            if(result == "HTTP_FORBIDDEN"){
                inputLay.setVisibility(View.VISIBLE);
                loadLay.setVisibility(View.GONE);
                pass.setError(activity.getText(R.string.warning_pass_wrong));
                return;
            }

            ZipInputStream zin;
            try {
                zin = new ZipInputStream(new FileInputStream(activity.getExternalCacheDir()
                        .getAbsolutePath() + "/products.zip"
                ));
                ZipEntry entry;
                while ((entry = zin.getNextEntry()) != null) {
                    // анализ entry
                    // считывание содежимого
                    String fileName = entry.getName();
                    File newFile = new File(activity.getExternalCacheDir()
                            .getAbsolutePath()
                            + File.separator + fileName);

                    FileOutputStream fos = new FileOutputStream(newFile);
                    byte[] buffer = new byte[1024];
                    int len;
                    while ((len = zin.read(buffer)) > 0) {
                        fos.write(buffer, 0, len);
                    }
                    zin.closeEntry();
                    fos.close();


                }

                zin.close();

            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            (new File(activity.getExternalCacheDir().getAbsolutePath() + "/products.zip")).delete();

            //тут переход на новое активити


            PreferenceManager.getDefaultSharedPreferences(activity).edit().putBoolean("logined", true).commit();
            activity.startActivity(new Intent(activity, MainActivity.class));

            activity.finish();
        }
    }





    public String POST(String url_){
        InputStream inputStream = null;
        String result = "";
        int responseCode;
        try {

            URL url = new URL(url_);

            HttpURLConnection httpConnection = (HttpURLConnection) url
                    .openConnection();
            // int responseCode = httpConnection.getResponseCode();

            httpConnection.setRequestMethod("POST");
            httpConnection.setDoInput(true);
            httpConnection.setDoOutput(true);

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("md5", utils.md5(
                    name.getEditableText().toString() + pass.getEditableText().toString()
            )));
            PreferenceManager.getDefaultSharedPreferences(activity).edit().putString("md5",
                    utils.md5(
                            name.getEditableText().toString() + pass.getEditableText().toString()
                    )).commit();
            OutputStream os = httpConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(getQuery(params));
            writer.flush();
            writer.close();
            os.close();

            httpConnection.connect();
            responseCode = httpConnection.getResponseCode();
            result += responseCode;

            if(responseCode==HttpURLConnection.HTTP_FORBIDDEN){
                return "HTTP_FORBIDDEN";
            }
            ///////


            // opens input stream from the HTTP connection
            inputStream = httpConnection.getInputStream();
            if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                return "sd_card_unmounted";
            }
            String saveFilePath = activity.getExternalCacheDir().getAbsolutePath() + "/products.zip";

            // opens an output stream to save into file
            FileOutputStream outputStream = new FileOutputStream(saveFilePath);

            int bytesRead = -1;
            byte[] buffer = new byte[4096];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            outputStream.flush();
            outputStream.close();
            inputStream.close();

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    public ApiGetDB(Activity activity, EditText name, EditText pass, View loadLay, View inputLay){
        this.name = name;
        this.pass = pass;
        this.loadLay = loadLay;
        this.inputLay = inputLay;
        this.activity = activity;
        sendRequest = new HttpAsyncTask();
    }

}
