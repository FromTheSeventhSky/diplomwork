package ru.fromtheseventhsky.mydyplom.utility;


import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;

public class MyDatabase extends SQLiteOpenHelper {

    private static final String VISITS_TABLE_CREATE =
            "create table visits (id integer primary key autoincrement, " +
                    "tp_id integer, " +
                    "factory_id integer, " +
                    "start integer, " +
                    "end integer, " +
                    "photo_start_1 BLOB, " +
                    "photo_start_2 BLOB, " +
                    "photo_start_3 BLOB, " +
                    "photo_start_4 BLOB, " +
                    "photo_start_5 BLOB, " +
                    "photo_end_1 BLOB, " +
                    "photo_end_2 BLOB, " +
                    "photo_end_3 BLOB, " +
                    "photo_end_4 BLOB, " +
                    "photo_end_5 BLOB, " +
                    "lon_start float, " +
                    "lat_start float, " +
                    "lon_end float, " +
                    "lat_end float, " +
                    "month_year text, " +
                    "status integer, " +
                    "done_percent numeric(10,2), " +
                    "comment text);";

    private static final String REPORTS_TABLE_CREATE =
            "create table reports (id integer primary key autoincrement, " +
                    "visit_id integer, " +
                    "goods_id integer, " +
                    "factory_id integer, " +
                    "report_chain_id integer, " +
                    "report_category_id integer, " +
                    "basic_count integer, " +
                    "count integer, " +
                    "basic_price numeric(10,2), " +
                    "real_price numeric(10,2), " +
                    "status integer);";

    public MyDatabase(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, context.getExternalCacheDir().getAbsolutePath()
                + File.separator + "/upload/" + File.separator
                + name, null, version);
    }

    public MyDatabase(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(VISITS_TABLE_CREATE);
        db.execSQL(REPORTS_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
