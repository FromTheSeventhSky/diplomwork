package ru.fromtheseventhsky.mydyplom.utility;


public class DataInGoods {

    private int idRow;              //идентификатор строки в бд

    private int id;                 //идентификатор товара
    private int factory_id;         //идентификатор производителя
    private String factory_name;    //имя производителя
    private String name;            //название товара
    private int article;            //артикул
    private float price;            //цена
    private boolean onStock;        //на складе
    private boolean onShelf;        //на полке

    public DataInGoods(int idRow, int id,
                       int factory_id,
                       String factory_name,
                       String name,
                       int article,
                       float price, boolean onStock, boolean onShelf) {
        this.idRow = idRow;
        this.id = id;
        this.factory_id = factory_id;
        this.factory_name = factory_name;
        this.name = name;
        this.article = article;
        this.price = price;
        this.onStock = onStock;
        this.onShelf = onShelf;
    }

    public int getIdRow() {
        return idRow;
    }

    public int getId() {
        return id;
    }

    public int getFactory_id() {
        return factory_id;
    }

    public String getFactory_name() {
        return factory_name;
    }

    public String getName() {
        return name;
    }

    public int getArticle() {
        return article;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public boolean isOnStock() {
        return onStock;
    }

    public void setOnStock(boolean onStock) {
        this.onStock = onStock;
    }

    public boolean isOnShelf() {
        return onShelf;
    }

    public void setOnShelf(boolean onShelf) {
        this.onShelf = onShelf;
    }
}
