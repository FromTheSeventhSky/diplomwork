package ru.fromtheseventhsky.mydyplom;

import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

import ru.fromtheseventhsky.mydyplom.adapters.ViewPagerCustomAdapter;
import ru.fromtheseventhsky.mydyplom.fragments.MainPageFragment;
import ru.fromtheseventhsky.mydyplom.fragments.MapFragment;
import ru.fromtheseventhsky.mydyplom.fragments.MyViewPager;
import ru.fromtheseventhsky.mydyplom.fragments.NavigationDrawerFragment;
import ru.fromtheseventhsky.mydyplom.utility.DataInTP;
import ru.fromtheseventhsky.mydyplom.utility.utils;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks, MainPageFragment.Interface {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private ActionBarDrawerToggle mDrawerToggle;
    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */

    private MyViewPager pager;
    private PagerAdapter pagerAdapter;
    private DrawerLayout mDrawerLayout;
    private MenuItem goToMapBtn;
    MainPageFragment mainPageFragment;
    MapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        getSupportActionBar().setTitle(R.string.title_main);




        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        //FragmentManager fragmentManager = getSupportFragmentManager();
        //fragmentManager.beginTransaction()
        //        .replace(R.id.container, MainPageFragment.newInstance())
        //        .commit();

        pager = (MyViewPager) findViewById(R.id.pager);
        pagerAdapter = new ViewPagerCustomAdapter(getSupportFragmentManager(),
                (mainPageFragment=MainPageFragment.newInstance()),
                (mapFragment=MapFragment.newInstance())
        );
        pager.setAdapter(pagerAdapter);

        pager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {

            }
        });

        utils.showGPSNotifications();

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_drawer, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {

                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
            }
        };

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if(state==ViewPager.SCROLL_STATE_SETTLING){
                    switch(pager.getCurrentItem()){
                        case 0:
                            mDrawerToggle.setDrawerIndicatorEnabled(true);
                            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                            getSupportActionBar().setTitle(R.string.title_main);
                            goToMapBtn.setVisible(true);
                            break;
                        case 1:
                            goToMapBtn.setVisible(false);
                            mDrawerToggle.setDrawerIndicatorEnabled(false);
                            getSupportActionBar().setTitle(R.string.title_map);
                            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                            break;
                    }
                }
            }
        });


        utils.openGPSConnection(this);




    }

    @Override
    public void onBackPressed() {
        switch(pager.getCurrentItem()){
            case 0:
                super.onBackPressed();
            break;
            case 1:
                pager.setCurrentItem(0);
            break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        utils.addGPSUpdates();
    }

    @Override
    protected void onPause() {
        super.onPause();
        utils.removeGPSUpdates();
    }



    @Override
    public void onNavigationDrawerNeedToggleSend(ActionBarDrawerToggle mDrawerToggle){
        this.mDrawerToggle = mDrawerToggle;
    }
    @Override
    public MyViewPager onMainPageFragmentNeedViewPager(){
        return pager;
    }
    @Override
     public MyViewPager onListViewItemClick(DataInTP data){
        mapFragment.goToMarker(data);
        return pager;
    }

    @Override
    public void onMapNeedData(ArrayList<DataInTP> data) {
        mapFragment.setData(data);
    }






    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        //actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowHomeEnabled(true);
        //actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDefaultDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            getMenuInflater().inflate(R.menu.main, menu);
            goToMapBtn = menu.findItem(R.id.action_open_map);
            goToMapBtn.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_open_map) {
            pager.setCurrentItem(1);
            return true;
        }

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        if (id == android.R.id.home) {
            switch(pager.getCurrentItem()){
                case 0:
                    return super.onOptionsItemSelected(item);

                case 1:
                    pager.setCurrentItem(0);
                    break;
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



}
