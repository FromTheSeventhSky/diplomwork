package ru.fromtheseventhsky.mydyplom.fragments;


import android.app.Activity;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

import ru.fromtheseventhsky.mydyplom.R;
import ru.fromtheseventhsky.mydyplom.adapters.VisitListViewAdapter;
import ru.fromtheseventhsky.mydyplom.loaders.LoaderGoods;
import ru.fromtheseventhsky.mydyplom.utility.DataInChains;
import ru.fromtheseventhsky.mydyplom.utility.DataInGoods;
import ru.fromtheseventhsky.mydyplom.utility.utils;

public class VisitFragment extends Fragment implements LoaderManager.LoaderCallbacks<ArrayList<Cursor>> {

    VisitInterface visitInterface;

    ListView lv;
    VisitListViewAdapter visitListViewAdapter;
    private Button find;
    private Button cancelFind;
    private EditText findTV;
    private int chain_id;
    private int category_id;
    private VisitFragment vf;

    ArrayList<Integer> quant;
    ArrayList<DataInGoods> dataInGoods = new ArrayList<DataInGoods>();
    ArrayList<DataInChains> dataInChains = new ArrayList<DataInChains>();
    ArrayList<Integer> indexes = new ArrayList<Integer>();

    private SQLiteDatabase uploadDB;

    public static VisitFragment newInstance(int chain_id, int category_id, SQLiteDatabase uploadDB) {
        VisitFragment fragment = new VisitFragment();
        fragment.setData(chain_id, category_id, uploadDB);
        return fragment;
    }
    private void setData(int chain_id, int category_id, SQLiteDatabase uploadDB){
        this.chain_id = chain_id;
        this.category_id = category_id;
        this.uploadDB = uploadDB;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.visit_fragment, null);

        lv = (ListView) v.findViewById(R.id.goodsList);


        findTV = (EditText) v.findViewById(R.id.vf_lv_findTextView);

        cancelFind = (Button) v.findViewById(R.id.vf_lv_cancel_find);

        vf = this;



        findTV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                visitListViewAdapter.resetFilter();
                visitListViewAdapter.getFilter().filter(findTV.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        cancelFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findTV.setText("");
                visitListViewAdapter.resetFilter();
                visitListViewAdapter.notifyDataSetChanged();
            }
        });

        getLoaderManager().initLoader(0, null, this).forceLoad();

        return v;

    }
    public void getList() {
        if (visitInterface != null) {
            visitInterface.getList(visitListViewAdapter.getData());
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            visitInterface = (VisitInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement VisitInterface");
        }
    }

    @Override
    public Loader<ArrayList<Cursor>> onCreateLoader(int id, Bundle args) {

        switch(id){
            case 0:
                return new LoaderGoods(getActivity().getApplicationContext(), utils.getDB(
                        new File(getActivity().getApplicationContext()
                                .getExternalCacheDir().getAbsolutePath(), "sync.sqlite")), chain_id+"", category_id+"", 0);
            case 1:
                return new LoaderGoods(getActivity().getApplicationContext(), uploadDB, quant, dataInGoods, indexes,
                         dataInChains, 1);
            case 2:
                int item=0;
                if(args!=null)
                    item = args.getInt("item");
                return new LoaderGoods(getActivity().getApplicationContext(), uploadDB, dataInGoods.get(item), 2);
            case 3:
                return new LoaderGoods(getActivity().getApplicationContext(), uploadDB, 3);
            default:
                return null;
        }

    }



    @Override
    public void onLoadFinished(Loader<ArrayList<Cursor>> loader, ArrayList<Cursor> data) {

        if(loader.getId()==0) {
            quant = new ArrayList<Integer>();
            dataInGoods = new ArrayList<DataInGoods>();
            dataInChains = new ArrayList<DataInChains>();
            indexes = new ArrayList<Integer>();

            boolean onShelf = false, onStock = false;

            Cursor c;
            c = data.get(1);
            c.moveToFirst();
            if (c.getCount() != 0)
                do {
                    indexes.add(c.getInt(c.getColumnIndex("goods_id")));
                    dataInChains.add(new DataInChains(
                            c.getInt(c.getColumnIndex("goods_id")),
                            c.getInt(c.getColumnIndex("chain_id")),
                            c.getInt(c.getColumnIndex("category_id")),
                            c.getInt(c.getColumnIndex("quant"))
                    ));

                } while (c.moveToNext());
            c.close();

            c = data.get(0);
            c.moveToFirst();



            int counter = 1;
            if (c.getCount() != 0) {
                do {
                    if (!dataInChains.isEmpty()){
                        int index = indexes.indexOf(c.getInt(c.getColumnIndex("id")));
                        if (index == -1)
                            continue;
                        else
                            quant.add(dataInChains.get(index).getQuant());
                    }
                    if(counter==1) {
                        SharedPreferences shp = getActivity().getSharedPreferences("visit_prefs", getActivity().MODE_PRIVATE);
                        SharedPreferences.Editor editShp = shp.edit();
                        editShp.putInt("factory_id", c.getInt(c.getColumnIndex("factory_id")));
                        editShp.commit();
                    }
                    dataInGoods.add(new DataInGoods(
                            counter,
                            c.getInt(c.getColumnIndex("id")),
                            c.getInt(c.getColumnIndex("factory_id")),
                            c.getString(c.getColumnIndex("factory_name")),
                            c.getString(c.getColumnIndex("name")),
                            c.getInt(c.getColumnIndex("article")),
                            c.getFloat(c.getColumnIndex("price")),
                            false,
                            false
                    ));

                    counter++;

                } while (c.moveToNext());
            }

            c.close();


            if(!getActivity().getSharedPreferences("visit_prefs", getActivity().MODE_PRIVATE)
                    .getBoolean("base_goods_uploaded", false))
                getLoaderManager().initLoader(1, null, this).forceLoad();
            else
                getLoaderManager().initLoader(3, null, this).forceLoad();



        }
        if(loader.getId()==1) {
            SharedPreferences shp = getActivity().getSharedPreferences("visit_prefs", getActivity().MODE_PRIVATE);
            SharedPreferences.Editor editShp = shp.edit();
            editShp.putBoolean("base_goods_uploaded", true);
            editShp.commit();
            getLoaderManager().initLoader(3, null, this).forceLoad();
        }
        if(loader.getId()==3) {

            Cursor c = data.get(0);
            c.moveToFirst();
            int counter = 0;
            boolean onStock = false, onShelf = false;
            if (c.getCount() != 0) {
                do {
                    switch (c.getInt(c.getColumnIndex("count"))) {
                        case 0:
                            onStock = false;
                            onShelf = false;
                            break;
                        case 1:
                            onStock = true;
                            onShelf = false;
                            break;
                        case 2:
                            onStock = true;
                            onShelf = true;
                            break;
                    }
                    dataInGoods.get(counter).setPrice(c.getFloat(c.getColumnIndex("real_price")));
                    dataInGoods.get(counter).setOnStock(onStock);
                    dataInGoods.get(counter).setOnShelf(onShelf);
                    counter++;

                } while (c.moveToNext());
            }
            c.close();

            visitListViewAdapter = new VisitListViewAdapter(getActivity(), dataInGoods);

            lv.setAdapter(visitListViewAdapter);

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    ShowProductInfo dialog = new ShowProductInfo();
                    dialog.setData((DataInGoods) visitListViewAdapter.getItem(position), position);
                    dialog.show(getFragmentManager(), "DataInGoods");
                }
            });
        }
    }


    @Override
    public void onLoaderReset(Loader<ArrayList<Cursor>> loader) {

    }

    public interface VisitInterface{
        public void getList(ArrayList<DataInGoods> data);
    }


    class ShowProductInfo extends DialogFragment {
        private DataInGoods data;
        private int position;

        public void setData(DataInGoods data, int position) {
            this.data = data;
            this.position = position;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.product_info_window, null);

            final TextView name = (TextView) view.findViewById(R.id.pi_product_name);
            final TextView company = (TextView) view.findViewById(R.id.pi_factory_name);
            final TextView article = (TextView) view.findViewById(R.id.pi_article_num);
            final EditText cost = (EditText) view.findViewById(R.id.pi_cost_et);
            final CheckBox stock = (CheckBox) view.findViewById(R.id.pi_on_stock_chb);
            final CheckBox shelf = (CheckBox) view.findViewById(R.id.pi_on_shelf_chb);

            Button accept = (Button) view.findViewById(R.id.pi_yes);
            Button cancel = (Button) view.findViewById(R.id.pi_no);

            name.setText(data.getName());
            company.setText(data.getFactory_name());
            article.setText(String.valueOf(data.getArticle()));
            cost.setText(String.valueOf(data.getPrice()));
            stock.setChecked(data.isOnStock());
            shelf.setChecked(data.isOnShelf());
            shelf.setEnabled(stock.isChecked() ? true : false);
            stock.setText(data.isOnStock()?getString(R.string.pi_yes):getString(R.string.pi_not));
            shelf.setText(data.isOnShelf()?getString(R.string.pi_yes):getString(R.string.pi_not));

            stock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((CheckBox)v).setText(((CheckBox) v).isChecked() ? getString(R.string.pi_yes) : getString(R.string.pi_not));
                    if(!((CheckBox)v).isChecked())
                        shelf.setChecked(false);
                    shelf.setEnabled(((CheckBox)v).isChecked() ? true : false);
                    if(!((CheckBox)v).isChecked())
                        shelf.setText(getString(R.string.pi_not));
                }
            });
            shelf.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((CheckBox)v).setText(((CheckBox)v).isChecked()?getString(R.string.pi_yes):getString(R.string.pi_not));
                }
            });

            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((DataInGoods) visitListViewAdapter.getItem(position)).setOnShelf(shelf.isChecked());
                    ((DataInGoods) visitListViewAdapter.getItem(position)).setOnStock(stock.isChecked());
                    ((DataInGoods) visitListViewAdapter.getItem(position)).setPrice(Float.parseFloat(cost.getText().toString()));
                    Bundle b = new Bundle();
                    b.putInt("item", position);
                    getLoaderManager().initLoader(2, b, vf).forceLoad();
                    visitListViewAdapter.notifyDataSetChanged();
                    getDialog().dismiss();
                }
            });
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDialog().dismiss();
                }
            });
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);



            getDialog().getWindow().setLayout(view.getWidth(), view.getHeight());

            return view;
        }
    }


}
