package ru.fromtheseventhsky.mydyplom.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import ru.fromtheseventhsky.mydyplom.R;


public class WriteCommentFragment extends Fragment {

    private EditText et;

    public static WriteCommentFragment newInstance() {
        WriteCommentFragment fragment = new WriteCommentFragment();
        return fragment;
    }

    public WriteCommentFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_write_comment, container, false);

        et = (EditText) v.findViewById(R.id.CommentET);


        return v;
    }

    public String getCommentText() {
        return et.getText().toString();
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }




}
