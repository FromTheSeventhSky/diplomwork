package ru.fromtheseventhsky.mydyplom.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.io.File;
import java.util.ArrayList;
import java.util.UUID;

import ru.fromtheseventhsky.mydyplom.R;
import ru.fromtheseventhsky.mydyplom.adapters.MainListViewAdapter;
import ru.fromtheseventhsky.mydyplom.utility.DataInTP;
import ru.fromtheseventhsky.mydyplom.loaders.LoaderTradePoints;
import ru.fromtheseventhsky.mydyplom.utility.utils;


public class MainPageFragment extends Fragment implements LoaderManager.LoaderCallbacks<ArrayList<DataInTP>> {

    private int LOADER_DB_WORK = 1;
    private Interface mListener;
    private ListView lv;
    private Button find;
    private Button cancelFind;
    private EditText findTV;
    private MainListViewAdapter adapter;
    private MyViewPager vp;

    public static MainPageFragment newInstance() {
        MainPageFragment fragment = new MainPageFragment();
        return fragment;
    }

    public MainPageFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.mainpage_fragment, container, false);
        lv = (ListView)v.findViewById(R.id.mainpageListView);
        lv.addHeaderView(inflater.inflate(R.layout.mainpage_listview_header, null));

        findTV = (EditText) lv.findViewById(R.id.mp_lv_findTextView);
        find = (Button) lv.findViewById(R.id.mp_lv_find_it);
        cancelFind = (Button) lv.findViewById(R.id.vf_lv_cancel_find);

        findTV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.resetFilter();
                adapter.getFilter().filter(findTV.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        UUID uuid = UUID.randomUUID();
        String str = uuid.toString();
        str = str.replaceAll("[-]*", "");

        getLoaderManager().initLoader(LOADER_DB_WORK, null, this).forceLoad();
        vp = mListener.onMainPageFragmentNeedViewPager();

        return v;
    }

    public MyViewPager onDbNeedInit() {
        if (mListener != null) {
            return mListener.onMainPageFragmentNeedViewPager();
        }
        return null;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (Interface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public Loader<ArrayList<DataInTP>> onCreateLoader(int id, Bundle args) {
        return new LoaderTradePoints(getActivity().getApplicationContext(), utils.getDB(
                new File(getActivity().getApplicationContext()
                .getExternalCacheDir().getAbsolutePath(), "sync.sqlite")));
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<DataInTP>> loader, ArrayList<DataInTP> data) {
        mListener.onMapNeedData(data);
        adapter = new MainListViewAdapter(getActivity(), data);

        utils.closeDB();
        cancelFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findTV.setText("");
                adapter.resetFilter();
                adapter.notifyDataSetChanged();
            }
        });
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mListener != null)
                    mListener.onListViewItemClick(
                        ((DataInTP)parent.getItemAtPosition(position)));
                vp.setCurrentItem(1);
            }
        });
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<DataInTP>> loader) {

    }


    public static interface Interface {
        public MyViewPager onMainPageFragmentNeedViewPager();
        public MyViewPager onListViewItemClick(DataInTP data);
        public void onMapNeedData(ArrayList<DataInTP> data);
    }

}
