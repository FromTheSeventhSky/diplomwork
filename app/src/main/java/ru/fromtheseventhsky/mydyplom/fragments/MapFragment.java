package ru.fromtheseventhsky.mydyplom.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import ru.fromtheseventhsky.mydyplom.R;
import ru.fromtheseventhsky.mydyplom.VisitActivity;
import ru.fromtheseventhsky.mydyplom.adapters.MainListViewAdapter;
import ru.fromtheseventhsky.mydyplom.utility.DataInTP;
import ru.fromtheseventhsky.mydyplom.utility.utils;


public class MapFragment extends Fragment {

    private SupportMapFragment mapFragment;
    private GoogleMap map;
    private Map<String, Cluster> clusters;
    private float zoomLevel;
    private double distanceBetweenAngles;
    private final double distanceBetweenAnglesDivideStandard = 6.959587568484861;
    private double stretchingRatio = 0;
    private double radiusOfCluster;
    private boolean firstStart = true;
    private ArrayList<DataInTP> data = null;
    private ArrayList<DataInTP> dataCopy;




    public class Cluster {
        private ArrayList<DataInTP> markers;
        private Marker marker;


        public Cluster(){
            markers = new ArrayList<DataInTP>();

        }

        public ArrayList<DataInTP> getList(){
            return markers;
        }
        public String getId(){
            return marker.getId();
        }

        public void add(DataInTP add){
            markers.add(add);
        }

        public DataInTP get(int index){
           return  markers.get(index);
        }

        public int size(){
            return markers.size();
        }

        public void removeMarker(){
            marker.remove();
        }

        public void addMarker(){
            if (map != null) {
                if(markers.size()>1) {

                    String size;
                    if(markers.size()>99){
                        size="99+";
                    } else {
                        size=""+markers.size();
                    }

                    marker = map.addMarker(new MarkerOptions()
                            .position(new LatLng(markers.get(0).lat, markers.get(0).lon))
                            .icon(
                                    BitmapDescriptorFactory
                                            .fromBitmap(
                                                    writeTextOnDrawable(R.drawable.green_dot,
                                                            size)
                                            )
                             )
                            .title("cluster"));


                } else {
                    marker = map.addMarker(new MarkerOptions()
                            .position(new LatLng(markers.get(0).lat, markers.get(0).lon))
                            .title(markers.get(0).chain_name));
                }
            }
        }


    }


    private Bitmap writeTextOnDrawable(int drawableId, String text) {

        Bitmap bm = BitmapFactory.decodeResource(getResources(), drawableId)
                .copy(Bitmap.Config.ARGB_8888, true);

        Typeface tf = Typeface.create("Helvetica", Typeface.BOLD);

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.BLACK);
        paint.setTypeface(tf);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize(convertToPixels(getActivity(), 15));

        Rect textRect = new Rect();
        paint.getTextBounds(text, 0, text.length(), textRect);

        Canvas canvas = new Canvas(bm);

        //If the text is bigger than the canvas , reduce the font size
        if(textRect.width() >= (canvas.getWidth() - 4))     //the padding on either sides is considered as 4, so as to appropriately fit in the text
            paint.setTextSize(convertToPixels(getActivity(), 14));        //Scaling needs to be used for different dpi's

        //Calculate the positions
        int xPos = (canvas.getWidth() / 2);     //-2 is for regulating the x position offset

        //"- ((paint.descent() + paint.ascent()) / 2)" is the distance from the baseline to the center.
        int yPos = (int) ((canvas.getHeight())/2 - (canvas.getHeight())/8 ) ;

        canvas.drawText(text, xPos, yPos, paint);

        return  bm;
    }



    public static int convertToPixels(Context context, int nDP){
        final float conversionScale = context.getResources().getDisplayMetrics().density;

        return (int) ((nDP * conversionScale) + 0.5f) ;

    }

    public static MapFragment newInstance() {
        MapFragment fragment = new MapFragment();
        return fragment;
    }

    public void setMarker(DataInTP data){
        if (map != null) {
            map.addMarker(new MarkerOptions()
                    .position(new LatLng(data.lat, data.lon))
                    .title("Hello world")
                    .snippet("Additional text"));


        }

    }

    public void goToMarker(DataInTP data){
        if (map != null) {
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(data.lat, data.lon))
                    .zoom(15)
                    .build();
            CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
            map.animateCamera(cameraUpdate);
        }

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        clusters = new HashMap<String, Cluster>();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.map_fragment,
                container, false);

        if (mapFragment == null) {
            mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        }
        if (map == null && mapFragment!= null) {
            map = mapFragment.getMap();
            map.getUiSettings().setZoomControlsEnabled(true);

            map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {
                    if(data!=null)
                        setVisibleMarkers(data);
                }
            });
            map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    if(marker.getTitle().equalsIgnoreCase("cluster")){

                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(marker.getPosition())
                                .zoom(zoomLevel)
                                .build();
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                        map.animateCamera(cameraUpdate, 350, null);




                        return false;
                    } else {
                        //marker.showInfoWindow();
                    }
                    return false;
                }
            });

            map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    if(marker.getTitle().equalsIgnoreCase("cluster")){
                        ClusterClickDialog dialog = new ClusterClickDialog();
                        dialog.setData(clusters.get(marker.getId()).getList());
                        dialog.show(getFragmentManager(), "d");
                    } else {
                        showDialog(clusters.get(marker.getId()).getList().get(0));
                    }



                }
            });
            map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                @Override
                public View getInfoWindow(Marker marker) {

                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    if(marker.getTitle().equalsIgnoreCase("cluster")){
                        TextView tv = new TextView(getActivity());
                        tv.setText(R.string.cluster_lv_info_window);
                        tv.setTag(marker.getId());
                        return tv;
                    }
                    return null;
                }
            });
            zoomLevel = map.getCameraPosition().zoom;
        }

        return rootView;
    }
    @Override
    public void onResume() {
        super.onResume();

    }

    public void setVisibleMarkers(ArrayList<DataInTP> data) {
        if (map != null) {
            //This is the current user-viewable region of the map
            LatLngBounds bounds = map.getProjection().getVisibleRegion().latLngBounds;


            if(stretchingRatio == 0){
                distanceBetweenAngles = Math.abs(bounds.northeast.latitude - bounds.southwest.latitude);
                stretchingRatio = distanceBetweenAngles/distanceBetweenAnglesDivideStandard;
            }


            if(Math.abs(zoomLevel - map.getCameraPosition().zoom)>0.005 || firstStart==true){
                firstStart = false;
                if(clusters.size()>0){
                    Iterator<Map.Entry<String, Cluster>> a = clusters.entrySet().iterator();

                    while(a.hasNext()){
                        clusters.get(a.next().getKey()).removeMarker();
                    }
                    clusters.clear();
                }
                zoomLevel = map.getCameraPosition().zoom;

                distanceBetweenAngles = Math.abs(bounds.northeast.longitude - bounds.southwest.longitude);

                radiusOfCluster = (distanceBetweenAngles/stretchingRatio);



                dataCopy = (ArrayList<DataInTP>) data.clone();


                for(int i = 0; i < dataCopy.size(); ){
                    DataInTP current = dataCopy.get(i);
                    Cluster cluster = new Cluster();
                    cluster.add(current);

                    for(int j = 1; j < dataCopy.size(); j++){
                        DataInTP tmp = dataCopy.get(j);
                        if(Math.abs(current.lat-tmp.lat)<radiusOfCluster &&
                        Math.abs(current.lon-tmp.lon)<radiusOfCluster) {
                            if (utils.getRadius(
                                    Math.abs(current.lat - tmp.lat),
                                    Math.abs(current.lon - tmp.lon)) < radiusOfCluster) {
                                cluster.add(tmp);
                            }
                        }
                    }
                    for(int k = 0; k<cluster.size(); k++){
                        dataCopy.remove(cluster.get(k));
                    }
                    cluster.addMarker();
                    clusters.put(cluster.getId(), cluster);

                }

            }

        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void setData(ArrayList<DataInTP> data){
        this.data = data;
        DataInTP tmp1, tmp2;

        //delete duplicate points

        /*for(int i = 0; i < data.size(); i++) {
            tmp1 = this.data.get(i);
            for(int j = data.size()-1; j > i; j--) {
                tmp2 = this.data.get(j);
                if(tmp1.lat==tmp2.lat && tmp1.lon == tmp2.lon)
                    this.data.remove(j);
            }
        }*/
        setVisibleMarkers(this.data);
    }


    public class ClusterClickDialog extends DialogFragment {

        private ArrayList<DataInTP> data;

        public void setData(ArrayList<DataInTP> data) {
            this.data = data;
        }


        @Override
          public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            getDialog().setTitle(R.string.cluster_lv_title);
            View v = inflater.inflate(R.layout.cluster_click_dialog, null);
            final ListView lv = (ListView) v.findViewById(R.id.clusterClickListView);
            lv.setAdapter(new MainListViewAdapter(getActivity(), data));
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    getDialog().dismiss();
                    showDialog((DataInTP) lv.getAdapter().getItem(position));
                }
            });
            return v;
        }
    }


    public void showDialog(final DataInTP data){
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.cluster_popup_title)
                .setPositiveButton(R.string.yes,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(getActivity(), VisitActivity.class);
                                intent.putExtra("tp_id", data.id);
                                intent.putExtra("chain_id", data.chain_id);
                                intent.putExtra("category_id", data.category_id);
                                intent.putExtra("start", Calendar.getInstance().getTimeInMillis()/1000);
                                intent.putExtra("lon_start", 1.1f);
                                intent.putExtra("lat_start", 1.1f);
                                startActivity(intent);
                                getActivity().finish();
                            }
                        })
                .setNegativeButton(R.string.no,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();

        /*
        if(utils.checkGPSEnabled()) {
            if(utils.checkLocationAvailable()) {
                new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.cluster_popup_title)
                        .setPositiveButton(R.string.yes,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(getActivity(), VisitActivity.class);
                                        intent.putExtra("tp_id", data.id);
                                        intent.putExtra("chain_id", data.chain_id);
                                        intent.putExtra("category_id", data.category_id);
                                        intent.putExtra("start", Calendar.getInstance().getTimeInMillis()/1000);
                                        intent.putExtra("lon_start", utils.getLongitude());
                                        intent.putExtra("lat_start", utils.getLatitude());
                                        startActivity(intent);
                                    }
                                })
                        .setNegativeButton(R.string.no,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                }).show();
            } else {
                Toast.makeText(getActivity(), R.string.location_wait_loc, Toast.LENGTH_LONG).show();
            }
        } else {
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.location_enable)
                    .setPositiveButton(R.string.yes,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    startActivity(new Intent(
                                            android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            })
                    .setNegativeButton(R.string.no,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();
        }

        */
    }


}
