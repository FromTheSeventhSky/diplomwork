package ru.fromtheseventhsky.mydyplom.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import ru.fromtheseventhsky.mydyplom.R;


public class CameraFragment extends Fragment {

    ImageButton cameraShoot;
    ImageButton btnLight;
    int lightMode=0;

    RelativeLayout photoPrevContainer;
    RelativeLayout allPhotoPrevContainer;
    ImageButton acceptPhoto;
    ImageButton cancelPhoto;

    ImageView previewPhotoLarge;

    ImageView previewLarge;

    ImageView previewSmallMain;
    ImageView previewSmall_1;
    ImageView previewSmall_2;
    ImageView previewSmall_3;
    ImageView previewSmall_4;
    ImageView previewSmall_5;
    Bitmap bmp;
    Bitmap prevBmp;

    ArrayList<Bitmap> allPhoto;

    int photoCount;

    SurfaceView surfaceView;
    Camera camera;
    MediaRecorder mediaRecorder;


    public void resetCamera(){
        photoCount = 0;
        previewSmall_1.setVisibility(View.GONE);
        previewSmall_2.setVisibility(View.GONE);
        previewSmall_3.setVisibility(View.GONE);
        previewSmall_4.setVisibility(View.GONE);
        previewSmall_5.setVisibility(View.GONE);
        photoPrevContainer.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        setFullscreen(true);
        Camera.Size bestSize = null;

        camera = Camera.open(0);

        setCameraDisplayOrientation(0);
        Camera.Parameters params = camera.getParameters();
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        params.getSupportedPictureSizes();
        //List<Camera.Size> pictureSizeList = params.getSupportedPictureSizes();
        int height = 480, width = 640;
        int pHeight = 480, pWidth = 640;

        //for(Camera.Size pictureSize : pictureSizeList){
        //    if(pictureSize.width  == 1280 && pictureSize.height == 720){
        //        height = 720;
        //        width = 1280;
        //    }
        //}
        //List<Camera.Size> previewSizeList = params.getSupportedPreviewSizes();

        //for(Camera.Size previewSize : previewSizeList){
        //    if(previewSize.width  == 1280 && previewSize.height == 720){
        //        pHeight = 720;
        //        pWidth = 1280;
        //    }
        //}


        params.setPictureSize(width, height);
        params.setPreviewSize(pWidth, pHeight);


        params.set("orientation", "portrait");
        params.set("rotation", 90);
        camera.setParameters(params);
        setPreviewSize();
    }




    private void setFullscreen(boolean on) {
        Window win = getActivity().getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        final int bits = WindowManager.LayoutParams.FLAG_FULLSCREEN;
        if (on) {
            winParams.flags |=  bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    @Override
    public void onPause() {
        super.onPause();
        releaseMediaRecorder();
        if (camera != null)
            camera.release();
        camera = null;
        setFullscreen(false);
    }

    private void releaseMediaRecorder() {
        if (mediaRecorder != null) {
            mediaRecorder.reset();
            mediaRecorder.release();
            mediaRecorder = null;
            camera.lock();
        }
    }

    private PhotoReturnListener mListener;


    public static CameraFragment newInstance() {
        CameraFragment fragment = new CameraFragment();
        return fragment;
    }

    public CameraFragment() {
        // Required empty public constructor
    }

    public void closePreview(){
        allPhotoPrevContainer.setVisibility(View.GONE);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_camera, container, false);

        cameraShoot = (ImageButton) v.findViewById(R.id.cameraShoot);

        btnLight = (ImageButton) v.findViewById(R.id.enableLight);

        previewLarge = (ImageView) v.findViewById(R.id.previewLarge);
        previewPhotoLarge = (ImageView) v.findViewById(R.id.previewPhotoLarge);
        previewSmallMain = (ImageView) v.findViewById(R.id.previewSmall);

        photoPrevContainer = (RelativeLayout) v.findViewById(R.id.photoPrevContainer);
        allPhotoPrevContainer = (RelativeLayout) v.findViewById(R.id.allPhotoPrevContainer);

        acceptPhoto = (ImageButton) v.findViewById(R.id.btnAcceptPhoto);
        cancelPhoto = (ImageButton) v.findViewById(R.id.btnCancelPhoto);

        previewSmall_1 = (ImageView) v.findViewById(R.id.cameraPrev1);
        previewSmall_2 = (ImageView) v.findViewById(R.id.cameraPrev2);
        previewSmall_3 = (ImageView) v.findViewById(R.id.cameraPrev3);
        previewSmall_4 = (ImageView) v.findViewById(R.id.cameraPrev4);
        previewSmall_5 = (ImageView) v.findViewById(R.id.cameraPrev5);

        allPhoto = new ArrayList<Bitmap>();

        previewSmall_1.setVisibility(View.GONE);
        previewSmall_2.setVisibility(View.GONE);
        previewSmall_3.setVisibility(View.GONE);
        previewSmall_4.setVisibility(View.GONE);
        previewSmall_5.setVisibility(View.GONE);

        photoPrevContainer.setVisibility(View.GONE);
        btnLight.setImageDrawable(getResources().getDrawable(R.drawable.light_no));


        previewSmallMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                allPhotoPrevContainer.setVisibility(View.VISIBLE);
                previewLarge.setImageBitmap(bmp);
            }
        });

        previewSmall_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previewLarge.setImageBitmap(allPhoto.get(0));
            }
        });
        previewSmall_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previewLarge.setImageBitmap(allPhoto.get(1));
            }
        });
        previewSmall_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previewLarge.setImageBitmap(allPhoto.get(2));
            }
        });
        previewSmall_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previewLarge.setImageBitmap(allPhoto.get(3));
            }
        });
        previewSmall_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previewLarge.setImageBitmap(allPhoto.get(4));
            }
        });


        btnLight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Camera.Parameters params = camera.getParameters();
                if(lightMode != 2)
                    lightMode++;
                else
                    lightMode = 0;
                switch (lightMode){
                    case 0:
                        btnLight.setImageDrawable(getResources().getDrawable(R.drawable.light_off_selector));
                        params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                        break;
                    case 1:
                        btnLight.setImageDrawable(getResources().getDrawable(R.drawable.light_auto_selector));
                        params.setFlashMode(Camera.Parameters.FOCUS_MODE_AUTO);
                        break;
                    case 2:
                        btnLight.setImageDrawable(getResources().getDrawable(R.drawable.light_on_selector));
                        params.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                        break;
                }


                camera.setParameters(params);

            }
        });

        acceptPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch(photoCount){
                    case 0:
                        previewSmall_1.setVisibility(View.VISIBLE);
                        previewSmall_1.setImageBitmap(Bitmap.createScaledBitmap(bmp, 108, 144, false));
                        break;
                    case 1:
                        previewSmall_2.setVisibility(View.VISIBLE);
                        previewSmall_2.setImageBitmap(Bitmap.createScaledBitmap(bmp, 108, 144, false));
                        break;
                    case 2:
                        previewSmall_3.setVisibility(View.VISIBLE);
                        previewSmall_3.setImageBitmap(Bitmap.createScaledBitmap(bmp, 108, 144, false));
                        break;
                    case 3:
                        previewSmall_4.setVisibility(View.VISIBLE);
                        previewSmall_4.setImageBitmap(Bitmap.createScaledBitmap(bmp, 108, 144, false));
                        break;
                    case 4:
                        previewSmall_5.setVisibility(View.VISIBLE);
                        previewSmall_5.setImageBitmap(Bitmap.createScaledBitmap(bmp, 108, 144, false));
                        break;

                }
                previewSmallMain.setImageBitmap(Bitmap.createScaledBitmap(bmp, 108, 144, false));
                allPhoto.add(bmp);
                prevBmp.recycle();
                photoCount++;


                if(photoCount>4){
                    resetCamera();
                    PhotoReturn(allPhoto);
                } else {
                    new AlertDialog.Builder(getActivity())
                            .setTitle(R.string.camera_popup_title)
                            .setPositiveButton(R.string.yes,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            photoPrevContainer.setVisibility(View.GONE);
                                            dialog.dismiss();
                                        }
                                    })
                            .setNegativeButton(R.string.no,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            for(int f = photoCount;f<5;f++){
                                                allPhoto.add(null);
                                            }
                                            resetCamera();
                                            PhotoReturn(allPhoto);
                                        }
                                    }).show();
                }

            }
        });

        cancelPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photoPrevContainer.setVisibility(View.GONE);
                bmp.recycle();
                prevBmp.recycle();
            }
        });



        cameraShoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                camera.autoFocus(new Camera.AutoFocusCallback() {
                    public void onAutoFocus(boolean success, Camera camera) {
                        if(success){
                            camera.takePicture(null, null, new Camera.PictureCallback() {
                                @Override
                                public void onPictureTaken(byte[] data, Camera camera) {
                                    try {
                                        bmp = RotateBitmap(BitmapFactory.decodeByteArray(data, 0, data.length), 90);
                                        int width = getActivity().getResources().getDisplayMetrics().widthPixels;
                                        int height = (width*bmp.getHeight())/bmp.getWidth();
                                        prevBmp = Bitmap.createScaledBitmap(bmp, width, height, true);

                                        ByteArrayOutputStream os = new ByteArrayOutputStream();
                                        prevBmp.compress(Bitmap.CompressFormat.JPEG, 50, os);

                                        previewPhotoLarge.setImageBitmap(BitmapFactory.decodeByteArray(os.toByteArray(), 0, os.size()));
                                        photoPrevContainer.setVisibility(View.VISIBLE);
                                        camera.startPreview();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    }
                });

            }
        });


        surfaceView = (SurfaceView) v.findViewById(R.id.surfaceView);
        SurfaceHolder holder = surfaceView.getHolder();
        holder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    camera.setPreviewDisplay(holder);
                    camera.startPreview();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format,
                                       int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {

            }
        });
        return v;
    }

    public Bitmap RotateBitmap(Bitmap source, float angle){
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        Bitmap bm = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
        source.recycle();
        return bm;
    }


    void setPreviewSize() {

        // получаем размеры экрана
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        boolean widthIsMax = display.getWidth() > display.getHeight();

        // определяем размеры превью камеры
        Camera.Size size = camera.getParameters().getPreviewSize();

        RectF rectDisplay = new RectF();
        RectF rectPreview = new RectF();

        // RectF экрана, соотвествует размерам экрана
        rectDisplay.set(0, 0, display.getWidth(), display.getHeight());

        // RectF первью
        if (widthIsMax) {
            // превью в горизонтальной ориентации
            rectPreview.set(0, 0, size.width, size.height);
        } else {
            // превью в вертикальной ориентации
            rectPreview.set(0, 0, size.height, size.width);
        }

        Matrix matrix = new Matrix();
        // подготовка матрицы преобразования
        matrix.setRectToRect(rectPreview, rectDisplay, Matrix.ScaleToFit.START);

        // преобразование
        matrix.mapRect(rectPreview);

        // установка размеров surface из получившегося преобразования
        surfaceView.getLayoutParams().height = (int) (rectPreview.bottom);
        surfaceView.getLayoutParams().width = (int) (rectPreview.right);
    }

    void setCameraDisplayOrientation(int cameraId) {
        // определяем насколько повернут экран от нормального положения
        int rotation = getActivity().getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result = 0;

        // получаем инфо по камере cameraId
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);

        // задняя камера
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
            result = ((360 - degrees) + info.orientation);
        } else
            // передняя камера
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                result = ((360 - degrees) - info.orientation);
                result += 360;
            }
        result = result % 360;
        camera.setDisplayOrientation(result);
    }

    public void PhotoReturn(ArrayList<Bitmap> allPhoto) {
        if (mListener != null) {
            mListener.PhotoReturn(allPhoto);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (PhotoReturnListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement PhotoReturnListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface PhotoReturnListener {
        public void PhotoReturn(ArrayList<Bitmap> allPhoto);
    }

}
