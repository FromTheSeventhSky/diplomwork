package ru.fromtheseventhsky.mydyplom.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import ru.fromtheseventhsky.mydyplom.R;
import ru.fromtheseventhsky.mydyplom.utility.DataInTP;


public class MainListViewAdapter extends BaseAdapter  implements Filterable {
    private LayoutInflater mInflater;
    private Context context;
    private ArrayList<DataInTP> dataInTP;
    ArrayList<DataInTP> originalDataInTP;
    private final static String TIME_FORMAT_LONG = "yyyy.MM.dd HH:mm:ss";

    public MainListViewAdapter(Context ctx, ArrayList<DataInTP> tp) {
        context = ctx;
        mInflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        dataInTP = tp;
        originalDataInTP = tp;
    }

    public void resetFilter(){
        dataInTP=originalDataInTP;
    }
    @Override
    public int getCount() {
        return dataInTP.size();
    }

    @Override
    public Object getItem(int position) {
        return dataInTP.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }




    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            view = mInflater.inflate(R.layout.mainpage_listview_item, parent, false);
        }

        DataInTP tp = (DataInTP) getItem(position);
        RelativeLayout root = (RelativeLayout) view.findViewById(R.id.mp_lv_item_root);
        TextView TVname = (TextView) view.findViewById(R.id.mp_lv_item_name);
        TextView TVaddress = (TextView) view.findViewById(R.id.mp_lv_item_address);
        TextView TVnext_visit_date = (TextView) view.findViewById(R.id.mp_lv_item_next_visit_date);
        TextView TVlast_visit_date = (TextView) view.findViewById(R.id.mp_lv_item_last_visit_date);

        TVname.setText(tp.chain_name+"");
        TVaddress.setText(tp.address+"");
        SimpleDateFormat sdf = new SimpleDateFormat(TIME_FORMAT_LONG, Locale.getDefault());
        TVnext_visit_date.setText(getTextNextDate(tp.lastvisit, tp.period));
        TVlast_visit_date.setText(sdf.format(new Date(tp.lastvisit*(long)1000)));

        if(tp.hasVisited==1){
            //root.setBackgroundColor(Color.rgb(179, 255, 208));
            //root.setBackgroundColor(context.getResources().getColor(R.color.lv_item_visited));
            root.setBackgroundResource(R.drawable.lv_item_visited);
        } else {
            //root.setBackgroundColor(Color.rgb(238, 238, 238));
            //root.setBackgroundColor(context.getResources().getColor(R.color.lv_item_not_visited));
            root.setBackgroundResource(R.drawable.lv_item_not_visited);
        }
        return view;
    }
    private String getTextNextDate(long oldDate, long nextDate){
        String answer="";
        long finalDate;
        if(Calendar.getInstance().getTimeInMillis() < (finalDate=(oldDate+nextDate*(long)86400)*(long)1000)){
            answer = ""+new SimpleDateFormat(TIME_FORMAT_LONG, Locale.getDefault())
                    .format(new Date(finalDate));
        } else {
            answer = context.getString(R.string.today);
        }

        return answer;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                // We implement here the filter logic
                if (constraint == null || constraint.length() == 0) {
                    // No filter implemented we return all the list
                    results.values = dataInTP;
                    results.count = dataInTP.size();
                }
                else {
                    ArrayList<DataInTP> _dataInTP = new ArrayList<DataInTP>();
                    for (DataInTP data : dataInTP) {
                        if (data.chain_name.toUpperCase().contains(constraint.toString().toUpperCase())||
                            data.address.toUpperCase().contains(constraint.toString().toUpperCase())||
                            data.category_name.toUpperCase().contains(constraint.toString().toUpperCase())||
                            data.name.toUpperCase().contains(constraint.toString().toUpperCase())
                           )
                            _dataInTP.add(data);
                    }

                    results.values = _dataInTP;
                    results.count = _dataInTP.size();

                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                    dataInTP = (ArrayList<DataInTP>) results.values;
                    notifyDataSetChanged();

            }
        };
    }

}
