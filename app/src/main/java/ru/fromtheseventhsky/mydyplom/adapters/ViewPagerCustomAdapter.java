package ru.fromtheseventhsky.mydyplom.adapters;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


import ru.fromtheseventhsky.mydyplom.fragments.MainPageFragment;
import ru.fromtheseventhsky.mydyplom.fragments.MapFragment;

public class ViewPagerCustomAdapter extends FragmentPagerAdapter {
    private MainPageFragment mainPageFragment;
    private MapFragment mapFragment;

    private int count = 2;

    public ViewPagerCustomAdapter(FragmentManager fm, MainPageFragment _mainPageFragment, MapFragment _mapFragment) {
        super(fm);
        mainPageFragment = _mainPageFragment;
        mapFragment = _mapFragment;
    }

    @Override
    public Fragment getItem(int position) {
        if(position==0)
         return mainPageFragment;
        return mapFragment;
    }



    @Override
    public int getCount() {
        return count;
    }


}
