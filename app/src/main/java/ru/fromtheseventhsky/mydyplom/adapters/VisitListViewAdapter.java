package ru.fromtheseventhsky.mydyplom.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;

import ru.fromtheseventhsky.mydyplom.R;
import ru.fromtheseventhsky.mydyplom.utility.DataInGoods;

public class VisitListViewAdapter extends BaseAdapter  implements Filterable {

    private ArrayList<DataInGoods> data;
    private ArrayList<DataInGoods> copyData;

    private LayoutInflater mInflater;
    private Context context;

    private final static String TIME_FORMAT_LONG = "yyyy.MM.dd HH:mm:ss";

    public VisitListViewAdapter(Context ctx, ArrayList<DataInGoods> data) {
        context = ctx;
        mInflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        copyData = data;
    }

    public ArrayList<DataInGoods> getData() {
        return data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = mInflater.inflate(R.layout.visit_fragment_listview_item, parent, false);
        }

        DataInGoods dataInGoods = (DataInGoods) getItem(position);

        TextView name_good = (TextView) view.findViewById(R.id.vf_name);
        TextView name_company = (TextView) view.findViewById(R.id.vf_company);
        //TextView article = (TextView) view.findViewById(R.id.vf_articul);
        //TextView cost = (TextView) view.findViewById(R.id.vf_cost);
        CheckBox onStock = (CheckBox) view.findViewById(R.id.vf_chb_on_stock);
        CheckBox onShelf = (CheckBox) view.findViewById(R.id.vf_chb_on_shelf);

        name_good.setText(dataInGoods.getName());
        name_company.setText(dataInGoods.getFactory_name());
        //article.setText(dataInGoods.getArticle()+"");
        //cost.setText(dataInGoods.getPrice()+"");
        onShelf.setChecked(dataInGoods.isOnShelf());
        onStock.setChecked(dataInGoods.isOnStock());



        return view;
    }


    public void resetFilter(){
        data=copyData;
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                // We implement here the filter logic
                if (constraint == null || constraint.length() == 0) {
                    // No filter implemented we return all the list
                    results.values = data;
                    results.count = data.size();
                }
                else {
                    ArrayList<DataInGoods> DataInGoods = new ArrayList<DataInGoods>();
                    for (DataInGoods dataObj : data) {
                        if (dataObj.getName().toUpperCase().contains(constraint.toString().toUpperCase())||
                                String.valueOf(dataObj.getArticle()).toUpperCase().contains(constraint.toString().toUpperCase())||
                                dataObj.getFactory_name().toUpperCase().contains(constraint.toString().toUpperCase())
                                )
                            DataInGoods.add(dataObj);
                    }

                    results.values = DataInGoods;
                    results.count = DataInGoods.size();

                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                data = (ArrayList<DataInGoods>) results.values;
                notifyDataSetChanged();

            }
        };
    }


}
